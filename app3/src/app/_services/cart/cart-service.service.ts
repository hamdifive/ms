import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Order } from 'src/app/_models/order';
@Injectable({
  providedIn: 'root'
})
export class CartService {
 


  constructor(private cookieService: CookieService) { }
  initCart(Order:Order){
    Order.total=this.getTotal(Order.orderItems)
    this.cookieService.delete(Order.resto);
    this.cookieService.set(Order.resto,JSON.stringify(Order),60);
    
  }
  deleteCart(id){
    this.cookieService.delete(id);
    
  }
  cartExist(id):Boolean{
    return this.cookieService.check(id);
     
  }

  getCart(id):Order{
    if(this.cartExist(id)){
    return new Order(JSON.parse(this.cookieService.get(id)));
    }
 else {   let order = new Order() ; order.total=0 ; return order;}
  }
  
  addToCart(id,item){
    let Order = this.getCart(id);
    Order.resto=id;
    Order.orderItems = [...Order.orderItems, item];
    Order.total = this.getTotal(Order.orderItems );
    this.initCart(Order);
  }
  getTotal(items):number{
    let total = 0;
    items.forEach(item => {
      total+=item.price*item.quantity
    });
    return total;
  }
 
}
