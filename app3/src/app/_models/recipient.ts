export class Recipient {
    _id:string;
    firstName:string;
    lastName:string;
    phoneNumber:string;
    cin:string;
    address:string;
    rib:string;
    email:string;
    password:string;
    verified:boolean;
    type:string;
    public constructor(init?: Partial<Recipient >) {
        Object.assign(this, init);
    }

}