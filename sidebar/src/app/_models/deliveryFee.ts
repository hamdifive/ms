export class DeliveryFee {

    _id:string;
    fee:number;
    priceKm:number;
    limit:number;
    feePercentage:number;
    profitPercentage:number;
    feePercentageOverLimit:number;
    orderLimit:number;



}