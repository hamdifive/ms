import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmptyRouteComponent } from './empty-route/empty-route.component';

import { FormsModule, ReactiveFormsModule} from '@angular/forms'; // <-- #1 import module

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HttpClientModule }    from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AdminListLivComponent } from './admin-list-liv/admin-list-liv.component';
import { DeliveryMenListComponent } from './delivery-men-list/delivery-men-list.component';

@NgModule({
  declarations: [
    AppComponent,
    EmptyRouteComponent,
    AdminListLivComponent,
    DeliveryMenListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    
    MDBBootstrapModule,
    HttpClientModule,
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }