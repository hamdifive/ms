import { NgModule } from '@angular/core';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { APP_BASE_HREF } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { DeliveryMenListComponent } from './delivery-men-list/delivery-men-list.component';
import {AdminListLivComponent} from './admin-list-liv/admin-list-liv.component';
import { AuthGuardService } from './_guards/auth-guard-admin.service';


const routes: Routes = [
  { path: 'app1', component: EmptyRouteComponent,
  
     children: [
      {
        path: 'exemple1',
        component: DeliveryMenListComponent
        
    },
    {
      path: 'exemple2',
      component: DeliveryMenListComponent
  }]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
  ],
})
export class AppRoutingModule { }
