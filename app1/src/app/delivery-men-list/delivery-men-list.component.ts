import { Component, OnInit } from '@angular/core';
import { DeliveryManService } from 'src/app/_services/deliveryMan/delivery-man.service';
import { DeliveryMan } from 'src/app/_models/deliveryMan';

@Component({
  selector: 'app-delivery-men-list',
  templateUrl: './delivery-men-list.component.html',
  styleUrls: ['./delivery-men-list.component.css']
})
export class DeliveryMenListComponent implements OnInit {
  deliveryMen:DeliveryMan[]=[];
  constructor(private deliveryManService:DeliveryManService) { }

  ngOnInit() {
    this.getDeliveryMen()
  }
  getDeliveryMen(){
    this.deliveryManService.getAllDeliveryMans().subscribe(deliveryMen=>{
      this.deliveryMen=deliveryMen;
    })
  }
  compareTime(value){
    let today= new Date();
    let date = new Date(value.updatedAt)
    let diff =today.getTime()-date.getTime()
    return( diff < 600000) && value.isConnected
  }

}
