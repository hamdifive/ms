import { Component } from '@angular/core';

@Component({
  selector: 'app1-empty-route',
  template: '<router-outlet></router-outlet>',
})
export class EmptyRouteComponent {
}
