import { Component, OnInit } from '@angular/core';
import { assetUrl } from 'src/single-spa/asset-url';
import { DeliveryManService } from 'src/app/_services/deliveryMan/delivery-man.service';

import { DeliveryMan } from 'src/app/_models/deliveryMan';
@Component({
  selector: 'app1-admin-list-liv',
  templateUrl: './admin-list-liv.component.html',
  styleUrls: ['./admin-list-liv.component.css']
})
export class AdminListLivComponent implements OnInit {

  
    deliveryMen:DeliveryMan[]=[];
    constructor(private deliveryManService:DeliveryManService) { }
  
    ngOnInit() {
      this.getDeliveryMen()
    }
    getDeliveryMen(){
      this.deliveryManService.getAllDeliveryMans().subscribe(deliveryMen=>{
        this.deliveryMen=deliveryMen;
      })
    }
    compareTime(value){
      let today= new Date();
      let date = new Date(value.updatedAt)
      let diff =today.getTime()-date.getTime()
      return( diff < 600000) && value.isConnected
    }
  
  
}
