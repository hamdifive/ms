import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminListLivComponent } from './admin-list-liv.component';

describe('AdminListLivComponent', () => {
  let component: AdminListLivComponent;
  let fixture: ComponentFixture<AdminListLivComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminListLivComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminListLivComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
