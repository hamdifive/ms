import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot ,Router } from '@angular/router';
import { Observable } from 'rxjs';

import { RecipientLoginService } from '../_services/recipient/recipient-login.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService2 {

  constructor(
    private _router: Router,private loginService:RecipientLoginService) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      let isLoggedIn = this.loginService.isLoggedIn();
      if (isLoggedIn) {
        this._router.navigate(['account']);
        return false;
      } else {
        return true;
      }
  }
}
