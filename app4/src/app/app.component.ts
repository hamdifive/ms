import { Component ,OnInit} from '@angular/core';
import { assetUrl } from 'src/single-spa/asset-url';
import { DeliveryManService } from 'src/app/_services/deliveryMan/delivery-man.service';
import { SenderLoginService } from 'src/app/_services/sender/sender-login.service';

import { DeliveryMan } from 'src/app/_models/deliveryMan';


@Component({
  selector: 'app4-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app4';
  yoshiUrl = assetUrl("yoshi.png");
  
  constructor(private loginService:SenderLoginService) { }

  ngOnInit() {
  }
 logout(){
   this.loginService.logoutAdmin()
 }
}
